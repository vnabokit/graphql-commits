# General info

A backend demo project that implements simple usage of GraphQL and Express.js with authenticational approache.

# Tools and technologies

- GraphQL
- Express.js
- SQLite (inmemory usage)
- TypeORM
- Axios
- TypeScript

# What does this project do:

Get a portion of commits from the `facebook/react` [GitHub's repository](https://api.github.com/repos/facebook/react/commits) and return its list according to the GraphQL query that was set by you.

# Setup

## Install the project locally

```
$ git clone https://gitlab.com/vnabokit/graphql-commits.git
$ cd graphql-commits
$ npm install
```

Rename `.env-example` to `.env`.

## Launch the project

```
$ npm start
```

If you see such logs:

```
Server is launched on port 4000
DB is initialized
Table is created
```

then the project is launched and ready to work.

## Usage

### Hello world

- Open `http://localhost:4000/` in a web-browser;
- In the `GraphiQL` UI interface that was opened, send query:

```
{
  helloWorld
}
```

- The response will be:

```
{
  "data": {
    "helloWorld": "Hello world!"
  }
}
```

### Get commits

- Open the Postman
- Set URL `http://localhost:4000/graphql`, method POST
- Select `GraphQL`
- Send such query:

```
mutation {
  generateApiKey
}
```

- The response will be like:

```
{
  "data": {
    "generateApiKey": "VDZM80W-GJP451R-JQKYG65-F9STG23"
  }
}
```

- In the Postman, use the generated auth key to set `X-API-KEY` header
- Send such query:

```
query{
    getCommits(page:2, perPage:4){
        sha,
        author{
            login
        },
    }
}
```

- You will get a list of commits. You may set different numbers of pages and commits per page. Also, feel free to add other fields to the query (see the full query schema below).

## Full query schema:

```
type Query {
    getCommits(page: Int=1, perPage: Int=10): [Commit]
  }

  type Commit {
    sha: String!
    message: String!
    htmlUrl: String!
    author: Author!
    date: String!
  }

  type Author {
    login: String!
  }

  type Mutation{
    generateApiKey: String!
  }
```
