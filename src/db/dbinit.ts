import "dotenv/config";
import { DataSource } from "typeorm";
import { ApiKey } from "./entities/apikey.entity";

export const DB = new DataSource({
  type: "sqlite",
  database: ":memory:",
  synchronize: false,
  entities: [ApiKey],
});

DB.initialize()
  .then((DB) => {
    console.log("DB is initialized");
    DB.query(
      "CREATE TABLE IF NOT EXISTS api_key (id INTEGER PRIMARY KEY, key TEXT NOT NULL, date TEXT NOT NULL)"
    )
      .then(() => {
        console.log("Table is created");
      })
      .catch((err) => {
        console.log(err.message);
      });
  })
  .catch((err) => {
    console.log(err.message);
  });

export default DB;
