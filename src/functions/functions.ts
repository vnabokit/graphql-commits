import { DataSource } from "typeorm";
import uuidAPIKey from "uuid-apikey";
import { ApiKey } from "../db/entities/apikey.entity";
import { CommitSimplified } from "../types/types";

export async function authorize(
  key: string | string[],
  DB: DataSource
): Promise<boolean> {
  key = Array.isArray(key) ? key[0] : key;
  if (key === "") {
    return false;
  }
  const foundKey = await DB.getRepository(ApiKey).findOneBy({ key });
  return foundKey === null ? false : true;
}

export function extractValues(obj: any): CommitSimplified {
  return {
    sha: obj.sha,
    htmlUrl: obj.html_url,
    message: obj.commit.message,
    date: obj.commit.author.date,
    author: {
      login: obj.author.login,
    },
  };
}

export function getApiKey(): string {
  return uuidAPIKey.create().apiKey;
}
