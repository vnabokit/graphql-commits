import axios from "axios";
import { Request } from "express";

import { CommitSimplified } from "../types/types";
import { ApiKey } from "../db/entities/apikey.entity";
import DB from "../db/dbinit";
import { extractValues, authorize, getApiKey } from "../functions/functions";

export const resolversHelloWorld = {
  helloWorld: () => "Hello world!",
};

export const resolversMain = {
  getCommits: async ({ page, perPage }: any, context: Request) => {
    if (!(await authorize(context.headers["x-api-key"] || "", DB))) {
      throw new Error("Not authorized");
    }
    const resp = await axios.get(
      `https://api.github.com/repos/facebook/react/commits?page=${page}&per_page=${perPage}`
    );
    let result: CommitSimplified[] = [];
    if (Array.isArray(resp.data)) {
      result = resp.data.map(extractValues);
    }
    return result;
  },

  generateApiKey: async () => {
    const apiKey = new ApiKey();
    apiKey.key = getApiKey();
    apiKey.date = new Date().toISOString();
    const apiKeySaved = await DB.manager.save(apiKey);
    return apiKeySaved.key;
  },
};
