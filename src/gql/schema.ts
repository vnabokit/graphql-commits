export const schemaHelloWorld = `
  type Query {
    helloWorld: String!
  }
`;

export const schemaMain = `
  type Query {
    getCommits(page: Int=1, perPage: Int=10): [Commit]
  }

  type Commit {
    sha: String!
    message: String!
    htmlUrl: String!
    author: Author!
    date: String!
  }

  type Author {
    login: String!
  }
  
  type Mutation{
    generateApiKey: String!
  }`;
