import "dotenv/config";
import express from "express";
import { graphqlHTTP } from "express-graphql";
import { buildSchema } from "graphql";

import { schemaHelloWorld, schemaMain } from "./gql/schema";
import { resolversHelloWorld, resolversMain } from "./gql/resolvers";

const app = express();

app.use(
  "/graphql",
  graphqlHTTP({
    schema: buildSchema(schemaMain),
    rootValue: resolversMain,
    graphiql: true,
  })
);

app.use(
  "/",
  graphqlHTTP({
    schema: buildSchema(schemaHelloWorld),
    rootValue: resolversHelloWorld,
    graphiql: true,
  })
);

const port = process.env.PORT;
app.listen(port, () => {
  console.log(`Server is launched on port ${port}`);
});
