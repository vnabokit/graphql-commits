export type CommitSimplified = {
  sha: string;
  message: string;
  htmlUrl: string;
  date: string;
  author: {
    login: string;
  };
};
